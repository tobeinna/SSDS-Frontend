import { Button, Grid, Typography } from "@mui/material";
import { Fragment } from "react";
import ServiceCard from "../card/service-card/ServiceCard";
import "./homepage-service-list.css"

const HomepageServiceList = () => {
  return (
    <Fragment>
      <Typography
        sx={{ marginLeft: "16px" }}
        variant="h6"
        component="div"
        my={1.5}
      >
        Danh mục sản phẩm
      </Typography>
      <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
        <Grid
          item
          xs={6}
          sm={4}
          lg={3}
          sx={{ display: "flex", justifyContent: "center" }}
        >
          <ServiceCard />
        </Grid>
        <Grid
          item
          xs={6}
          sm={4}
          lg={3}
          sx={{ display: "flex", justifyContent: "center" }}
        >
          <ServiceCard />
        </Grid>
        <Grid
          item
          xs={6}
          sm={4}
          lg={3}
          sx={{ display: "flex", justifyContent: "center" }}
        >
          <ServiceCard />
        </Grid>
        <Grid
          item
          xs={6}
          sm={4}
          lg={3}
          sx={{ display: "flex", justifyContent: "center" }}
        >
          <ServiceCard />
        </Grid>
        <Grid
          item
          xs={6}
          sm={4}
          lg={3}
          sx={{ display: "flex", justifyContent: "center" }}
        >
          <ServiceCard />
        </Grid>
        <Grid
          item
          xs={6}
          sm={4}
          lg={3}
          sx={{ display: "flex", justifyContent: "center" }}
        >
          <ServiceCard />
        </Grid>
        <Grid
          item
          xs={6}
          sm={4}
          lg={3}
          sx={{ display: "flex", justifyContent: "center" }}
        >
          <ServiceCard />
        </Grid>
        <Grid
          item
          xs={6}
          sm={4}
          lg={3}
          sx={{ display: "flex", justifyContent: "center" }}
        >
          <ServiceCard />
        </Grid>
        <Grid
          item
          xs={6}
          sm={4}
          lg={3}
          sx={{ display: "flex", justifyContent: "center" }}
        >
          <ServiceCard />
        </Grid>
      </Grid>
      <div
        className="button-container"
        style={{ display: "flex", justifyContent: "center" }}
      >
        <Button
          className="more-button"
          variant="contained"
          my={1.5}
          sx={{ color: "white", bgcolor: "#D4AE80", borderRadius: "25px" }}
        >
          XEM TẤT CẢ
        </Button>
      </div>
    </Fragment>
  );
};

export default HomepageServiceList;
