import { ListItem, ListItemButton, Typography } from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import { Fragment} from "react";
import "./category.css";

const VerticalCategory = () => {
  const [categoriesState, setCategoriesState] = useState([]);

//   const axios = require('axios').default;

//   const instance = axios.create({
//     baseURL: 'https://http://localhost:8088/',
//     timeout: 1000,
//     headers: {'X-Custom-Header': 'foobar'}
//   });

// // Make a request for a user with a given ID
//   instance.get('api/web/v1/category')
//   .then(function (response) {
//     // handle success
//     console.log(response);
//   })
//   .catch(function (error) {
//     // handle error
//     console.log(error);
//   })

  useEffect(() => {
    const getCategories = async () => {
      try {
        const res = await axios.get(`http://localhost:8088/api/web/v1/category`)
        console.log(res.data.data);
        setCategoriesState(res.data.data);
      } catch (error) {
        console.log(error.message);
      }
    }

    getCategories()
  }, [])
  
  return (
    <Fragment>
      <ListItem
        sx={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
        }}
      >
        {categoriesState.map((category, key) => (
          <ListItemButton
            className="category-button"
            component="a"
            key={key}
            sx={{ width: "f" }}
          >
            <Typography className="category-title" textAlign="left">
              {category.name}
            </Typography>
          </ListItemButton>
        ))}
      </ListItem>
    </Fragment>
  );
};

export default VerticalCategory;
