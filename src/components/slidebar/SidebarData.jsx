import React from 'react';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';
import * as RiIcons from 'react-icons/ri';


export const SidebarData = [
    {
        title : "Trang chủ",
        icon: <AiIcons.AiFillHome />,
        path: "/home",   
    },
    {
        title : "Thống kê",
        icon: <AiIcons.AiFillHdd />,
        path: "/dashbord"
    },
    {
        title : "Quản lí dữ liệu",
        icon: <AiIcons.AiFillSetting />,
        path: "/bento",
        iconClosed: <RiIcons.RiArrowDownSFill />,
        iconOpened: <RiIcons.RiArrowUpSFill />,
        subNav: [
            {
              title: 'Quản lí các loại dịch vụ',
              path: '/bento/bento1',
              icon: <IoIcons.IoIosPaper />,
              cName: 'sub-nav'
            },
            {
              title: 'Quản lí các cơ sở',
              path: '/bento/bento2',
              icon: <IoIcons.IoIosPaper />,
              cName: 'sub-nav'
            },
            {
              title: 'Quản lí các dịch vụ',
              path: '/bento/bento3',
              icon: <IoIcons.IoIosPaper />,
              cName: 'sub-nav'
            },
            {
              title: 'Quản lí các thiết bị',
              path: '/bento/bento4',
              icon: <IoIcons.IoIosPaper />,
              cName: 'sub-nav'
            },
            {
              title: 'Quản lí các bài đăng',
              path: '/bento/bento5',
              icon: <IoIcons.IoIosPaper />,
              cName: 'sub-nav'
            },
            {
              title: 'Quản lí các cấu hình',
              path: '/bento/bento6',
              icon: <IoIcons.IoIosPaper />,
              cName: 'sub-nav'
            },
            {
              title: 'Quản lí mã giảm giá',
              path: '/bento/bento7',
              icon: <IoIcons.IoIosPaper />,
              cName: 'sub-nav'
            },
            {
              title: 'Quản lí câu hỏi SCA',
              path: '/bento/bento8',
              icon: <IoIcons.IoIosPaper />,
              cName: 'sub-nav'
            },
            {
              title: 'Quản lí chuyên viên',
              path: '/bento/bento9',
              icon: <IoIcons.IoIosPaper />,
              cName: 'sub-nav'
            }
          ]
    }
 
 ]
    
