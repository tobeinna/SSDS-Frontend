import { Button, Grid, Paper } from "@mui/material";
import { useContext } from "react";
import { BookingContext } from "../../context/BookingContext";

const TimeList = () => {

  const {timestampState, selectedTimestamp, setSelectedTimestamp, setIsFinishable} = useContext(BookingContext)

  const handleChooseTime = (timestamp) => {
    setSelectedTimestamp(timestamp)
    setIsFinishable(true);
    // console.log("timestamp", selectedTimestamp);
  }
  // console.log(list);

  return (
    <Paper style={{ maxHeight: 400, overflow: "auto" }}>
      <Grid
        container
        spacing={{ xs: 2, sm: 3, md: 3 }}
        columns={{ xs: 4, sm: 8, md: 12 }}
      >
        {timestampState?.map((time, index) => (
          <Grid
            item
            sx={{ display: "flex", justifyContent: "center" }}
            xs={2}
            sm={2}
            md={3}
            key={index}
          >
            <Button variant="outlined" onClick={() => {handleChooseTime(time.timestamp)}}>{time.time}</Button>
          </Grid>
        ))}
      </Grid>
    </Paper>
  );
};

export default TimeList;