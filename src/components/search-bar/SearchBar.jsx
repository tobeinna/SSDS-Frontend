import { TextField } from "@mui/material";
// import SearchIcon from "@mui/icons-material/Search";
// import { Fragment } from "react";
import "./search-bar.css";

const SearchBar = (props) => (
  <div className="search-bar-container">
    <form className="search-form">
      <TextField
        id="search-bar"
        className="text"
        label={props.label}
        variant="outlined"
        placeholder={props.placeholder}
        size="small"
      />
      {/* <IconButton type="submit" aria-label="search">
        <SearchIcon />
      </IconButton> */}
    </form>
  </div>
);

export default SearchBar;
