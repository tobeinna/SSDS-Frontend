import { List, ListItemButton, ListItemText, Typography } from "@mui/material";
import { useContext } from "react";
import { BookingContext } from "../../context/BookingContext";
import "./booking-branch-list.css";

const BookingBranchList = (props) => {
  // console.log(props);
  const {selectedBranch, setSelectedBranch, setIsNextable1} = useContext(BookingContext);

  var currentBranch;

  const selectBranch = (branch) => {
    setSelectedBranch(branch);
    currentBranch = branch
    setIsNextable1(true);
    console.log("branch");
    console.log(branch);
    console.log("currentBranch");
    console.log(currentBranch);
  }
  return (
    <div>
      <List className="list-container">
        {props?.branches?.map((branch, key) => (
          <ListItemButton
          onClick={() => selectBranch(branch)}
          key={key}
            className="list-btn"
            sx={{ border: 2, 
              borderColor: (currentBranch?.code === branch?.code)
            ? "#FFFFFF"
            : "#000000" , 
            borderRadius: 1 }}
          >
            <ListItemText className="branch-title">{branch.name}</ListItemText>
            <Typography className="branch-distance" >{branch?.distance?.text}</Typography>
            {/* <ListItemText className="brach-distance">Chi nhánh 1</ListItemText> */}
          </ListItemButton>
        ))}
      </List>
    </div>
  );
};

export default BookingBranchList;
