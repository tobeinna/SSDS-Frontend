import React from "react";
import {
Box,
Container,
Row,
Column,
FooterLink,
Heading,
} from "./FooterStyles";

const Footer = () => {
return (
	<Box className="footer-container">
	<Container>
		<Row>
		<Column>
			<Heading>LOGO</Heading>
		</Column>
		<Column>
			<Heading>Dịch vụ</Heading>
			<FooterLink href="#">Trị mụn</FooterLink>
			<FooterLink href="#">Triệt lông</FooterLink>
			<FooterLink href="#">Xóa sẹo</FooterLink>
			<FooterLink href="#">Tẩy tế bào chết</FooterLink>
		</Column>
		<Column>
			<Heading>Danh sách chi nhánh</Heading>
			<FooterLink href="#">Chi nhánh 1</FooterLink>
			<FooterLink href="#">Chi nhánh 1</FooterLink>
			<FooterLink href="#">Chi nhánh 1</FooterLink>
			<FooterLink href="#">Chi nhánh 1</FooterLink>
		</Column>
		<Column>
			<Heading>Liên hệ</Heading>
			<FooterLink href="#">
			<i className="fab fa-facebook-f">
				<span style={{ marginLeft: "10px" }}>
				Facebook
				</span>
			</i>
			</FooterLink>
			<FooterLink href="#">
			<i className="fab fa-instagram">
				<span style={{ marginLeft: "10px" }}>
				Instagram
				</span>
			</i>
			</FooterLink>
			<FooterLink href="#">
			<i className="fab fa-twitter">
				<span style={{ marginLeft: "10px" }}>
				Twitter
				</span>
			</i>
			</FooterLink>
			<FooterLink href="#">
			<i className="fab fa-youtube">
				<span style={{ marginLeft: "10px" }}>
				Youtube
				</span>
			</i>
			</FooterLink>
		</Column>
		</Row>
	</Container>
	</Box>
);
};
export default Footer;
