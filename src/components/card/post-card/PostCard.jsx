import "./post-card.css";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import {CardActionArea} from "@mui/material";

const PostCard = () => {
  return (
    <Card className="post-card" my={4} sx={{ textAlign: "center", margin: "0 auto" }}>
      <CardActionArea>
        <CardMedia
          component="img"
          image="https://bucket.nhanh.vn/store/24031/art/290632943_5303234649763331_4127607906404809144_n.jpg"
          alt="Mặt nạ chuẩn bài dành cho mùa hè"
        />
        <CardContent>
          <Typography className="post-date" variant="body2" textAlign={"left"} component="div">
            11.07.2022
          </Typography>
          <Typography className="post-title" gutterBottom  textAlign={"left"}>
            Mặt nạ chuẩn bài dành cho mùa hè
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default PostCard;
