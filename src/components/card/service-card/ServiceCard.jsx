import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";
import "./service-card.css";

const ServiceCard = () => {
  return (
    <Card className="service-card" sx={{ textAlign: "center" }}>
      <CardActionArea>
        <CardMedia
          component="img"
          image="https://bucket.nhanh.vn/store/24031/ps/20220808/08082022040820_thumb_web_01__1__thumb.jpg"
          alt="Deep Cleansing - Làm Sạch Chuyên Sâu"
        />
        <CardContent>
          <Typography className="service-title" gutterBottom variant="body2">
            Deep Cleansing - Làm Sạch Chuyên Sâu
          </Typography>
          <Typography className="service-price" variant="h5" component="div">
            550.000
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default ServiceCard;
