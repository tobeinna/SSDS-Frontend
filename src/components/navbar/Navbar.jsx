import * as React from "react";
import "./navbar.css";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import { Drawer } from "@mui/material";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const Navbar = () => {
  const [anchorElNav, setAnchorElNav] = useState(null);
  const [anchorElUser, setAnchorElUser] = useState(null);
  // const { to, anchorOrigin, ...rest } = this.props;

  const navigate = useNavigate();

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <AppBar
      className="header-navbar"
      position="sticky"
      sx={{ bgcolor: "black", color: "#acacac" }}
    >
      <Container maxWidth="xl">
        <Toolbar className="toolbar" disableGutters>
          <Typography
            variant="h6"
            align="left"
            noWrap
            component="a"
            href="/"
            sx={{
              mr: 2,
              display: { xs: "none", md: "flex" },
              fontFamily: "monospace",
              fontWeight: 700,
              letterSpacing: ".3rem",
              color: "inherit",
              textDecoration: "none",
              flexGrow: 1,
            }}
          >
            LOGO
          </Typography>

          <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
            
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Drawer
              id="menu-appbar"
              anchorel={anchorElNav}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
              }}
              keepMounted
              transformorigin={{
                vertical: "top",
                horizontal: "left",
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: "block", md: "none" },
              }}
              PaperProps={{
                sx: { width: "150px" },
              }}
            >
              <MenuItem key="service" onClick={handleCloseNavMenu}>
                <Typography textAlign="right">Dịch vụ</Typography>
              </MenuItem>
              <MenuItem key="post" onClick={handleCloseNavMenu}>
                <Typography textAlign="right">Bài viết</Typography>
              </MenuItem>
              <MenuItem key="help" onClick={handleCloseNavMenu}>
                <Typography textAlign="right">Tư vấn</Typography>
              </MenuItem>
              <MenuItem key="branch-list" onClick={handleCloseNavMenu}>
                <Typography textAlign="right">Danh sách cơ sở</Typography>
              </MenuItem>
            </Drawer>
          </Box>
          <Typography
            variant="h5"
            noWrap
            component="a"
            href="/"
            sx={{
              mr: 2,
              display: { xs: "flex", md: "none" },
              flexGrow: 1,
              fontFamily: "monospace",
              fontWeight: 700,
              letterSpacing: ".3rem",
              color: "inherit",
              textDecoration: "none",
            }}
          >
            LOGO
          </Typography>
          <Box
            sx={{
              display: { xs: "none", md: "flex", justifyContent: "center" },
            }}
          >
            <Button
              className="nav-item"
              key="service"
              onClick={handleCloseNavMenu}
              sx={{ color: "white", display: "block" }}
            >
              Dịch vụ
            </Button>
            <Button
              className="nav-item"
              key="post"
              onClick={handleCloseNavMenu}
              sx={{ color: "white", display: "block" }}
            >
              Bài viết
            </Button>
            <Button
              className="nav-item"
              key="help"
              onClick={handleCloseNavMenu}
              sx={{ color: "white", display: "block" }}
            >
              Tư vấn
            </Button>
            <Button
              className="nav-item"
              key="branch-list"
              onClick={handleCloseNavMenu}
              sx={{ color: "white", display: "block" }}
            >
              Danh sách cơ sở
            </Button>
          </Box>

          <Box sx={{ flexGrow: 0 }}>
            <Tooltip title="Open settings">
              <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                <Avatar alt="Remy Sharp" src="/static/images/avatar/2.jpg" />
              </IconButton>
            </Tooltip>
            <Drawer
              sx={{ mt: "45px", display: { xs: "block", md: "none" } }}
              id="menu-appbar"
              anchor="right"
              anchorel={anchorElUser}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformorigin={{
                vertical: "top",
                horizontal: "right",
              }}
              width={200}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
              PaperProps={{
                sx: { width: "150px" },
              }}
            >
              <MenuItem key="account" onClick={handleCloseUserMenu}>
                <Typography textAlign="center">Tài khoản</Typography>
              </MenuItem>
              <MenuItem key="booking" onClick={() => {
                navigate('/booking');
                handleCloseUserMenu()}}>
                <Typography textAlign="center">Đặt lịch</Typography>
              </MenuItem>
              <MenuItem key="history" onClick={handleCloseUserMenu}>
                <Typography textAlign="center">Lịch sử</Typography>
              </MenuItem>
              <MenuItem key="logout" onClick={() => {
                navigate('/login');
                handleCloseUserMenu()}}>
                <Typography textAlign="center">Đăng xuất</Typography>
              </MenuItem>
            </Drawer>
          </Box>
          <Box sx={{ flexGrow: 0 }}>
            <Menu
              sx={{ mt: "45px", display: { xs: "none", md: "flex" } }}
              id="menu-appbar"
              anchor="right"
              anchorel={anchorElUser}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformorigin={{
                vertical: "top",
                horizontal: "right",
              }}
              width={200}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
            >
              <MenuItem key="account" onClick={handleCloseUserMenu}>
                <Typography textAlign="center">Tài khoản</Typography>
              </MenuItem>
              <MenuItem key="booking" onClick={() => {
                navigate('/booking');
                handleCloseUserMenu()}}>
                <Typography textAlign="center">Đặt lịch</Typography>
              </MenuItem>
              <MenuItem key="history" onClick={handleCloseUserMenu}>
                <Typography textAlign="center">Lịch sử</Typography>
              </MenuItem>
              <MenuItem key="logout" onClick={() => {
                navigate('/login');
                handleCloseUserMenu()}}>
                <Typography textAlign="center">Đăng xuất</Typography>
              </MenuItem>
            </Menu>
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default Navbar;
