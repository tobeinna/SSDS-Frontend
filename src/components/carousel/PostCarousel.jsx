import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import PostCard from "../card/post-card/PostCard";
import { Typography } from "@material-ui/core";
// import { Fragment } from "react";
import "./carousel.css";

const PostCarousel = () => {
  var settings = {
    arrows: false,
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
  };

  return (
    <div className="post-carousel-container">
      <div className="post-carousel-header">
        <Typography
          sx={{ textAlign: "center" }}
          variant="h5"
          component="div"
          my={3}
        >
          Bài viết gần đây
        </Typography>
      </div>
      <div>
        <Slider {...settings}>
          <PostCard />
          <PostCard />
          <PostCard />
          <PostCard />
          <PostCard />
          <PostCard />
          <PostCard />
          <PostCard />
          <PostCard />
          <PostCard />
          <PostCard />
        </Slider>
      </div>
    </div>
  );
};

export default PostCarousel;
