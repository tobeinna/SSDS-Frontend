import * as React from "react";
// import TinySlider from "tiny-slider-react";
// import "tiny-slider/dist/tiny-slider.css";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
// import 'lazysizes';
// import 'lazysizes/plugins/parent-fit/ls.parent-fit';
// import Carousel from "react-material-ui-carousel";
// import pic1 from "../../assets/carousel-picture/carousel-picture-1.jpg";
// import pic2 from "../../assets/carousel-picture/carousel-picture-2.png";
// import pic3 from "../../assets/carousel-picture/carousel-picture-3.png";

const HomepageCarousel = () => {
  var items = [
    {
      href: "/",
      picture: "https://bucket.nhanh.vn/store/24031/bn/Trie____t_lo__ng.jpeg",
    },
    {
      href: "/",
      picture:
        "https://bucket.nhanh.vn/store/24031/bn/THE___LA__M_DE__P_QUYE____N_NA__NG.jpeg",
    },
  ];

  var settings = {
    arrows: false,
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  return (
    <div>
      <Slider {...settings}>
        {items.map((el, key) => (
          <div className="container" key={key}>
            {/* <a href={el.href}> */}
              <img style={{ width: "100%" }} src={el.picture} alt="" />
            {/* </a> */}
          </div>
        ))}
      </Slider>
    </div>
  );
};

export default HomepageCarousel;
