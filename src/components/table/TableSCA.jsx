import React from "react";
import { Button } from "@mui/material";
import './Table.css'


const Tablesca = ({ data, column }) => {

    return (

        <table>
            <thead>

                <tr>
                    {column.map((item, index) => <TableHeadItem key={index} item={item} />)}
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>

                {data.map((item, index) => <TableRow key={index} item={item} column={column} />)}

            </tbody>
        </table>

    )
}

const TableHeadItem = ({ item }) => <th>{item.heading}</th>
const TableRow = ({ item }) => {
    console.log(item);
    return <tr>
        <td>
            {item.id}
        </td>
        <td>
            {item.question}
        </td>
        <td>
            {item.order}
        </td>
        <td>
            {item.isRequire}
        </td>
        <td>
            {item.Form}
        </td>
        <td>
            <Button className="edit-btn" variant="contained" color="success">Edit</Button>
        </td>
    </tr>
}

export default Tablesca