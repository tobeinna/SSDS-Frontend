import { Fragment } from "react";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";
import "./default-layout.css"

function DefaultLayout({ children }) {
  return (
    <div className="default-layout-container">
      <Fragment>
        <Header />
      </Fragment>
      <div className="main">{children}</div>
      <Fragment>
        <Footer />
      </Fragment>
    </div>
  );
}
export default DefaultLayout;
