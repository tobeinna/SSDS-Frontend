import { TextField } from "@mui/material";
// import SearchIcon from "@mui/icons-material/Search";
import { Fragment } from "react";
import './search-bar.css';



const SearchBar = () => (
  <Fragment className="search-bar-container">
    <form className="search-form">
      <TextField
        id="search-bar"
        className="text"
        label="Tìm kiếm"
        variant="outlined"
        placeholder="Tìm..."
        size="small"
      />
    </form>
  </Fragment>
);

export default SearchBar;