import "./App.css";
import { publicRoutes } from "./routes";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import DefaultLayout from "./layout/DefaultLayout";
import { Fragment } from "react";
import { StrictMode } from "react";
import { BookingProvider } from "./context/BookingContext";

function App() {
  return (
    <StrictMode>
      <BookingProvider>
        <Router>
          {/* <div className="App"> */}
          <Routes>
            {publicRoutes.map((route, index) => {
              const Page = route.component;

              let Layout = DefaultLayout;

              if (route.layout) {
                Layout = route.layout;
              } else if (route.layout === null) {
                Layout = Fragment;
              }

              return (
                <Route
                  key={index}
                  path={route.path}
                  element={
                    <Layout>
                      <Page />
                    </Layout>
                  }
                />
              );
            })}
          </Routes>
          {/* </div> */}
        </Router>
      </BookingProvider>
    </StrictMode>
  );
}

export default App;
