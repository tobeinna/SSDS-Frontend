import {
  Button,
  Divider,
  List,
  ListItem,
  ListItemText,
  ListItemButton,
  Paper,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import { useContext } from "react";
// import axios from "axios";
import "./booking-service.css";
import { BookingContext } from "../../../../context/BookingContext";

const style = {
  width: "100%",
  maxWidth: 360,
  bgcolor: "background.paper",
};

const BookingService = () => {
  const { selectedServicesList, servicesList, selectService, unselectService} =
    useContext(BookingContext);

  const currencyFormatter = new Intl.NumberFormat("vi-VN", {
    style: "currency",
    currency: "VND",
  });

  return (
    <div className="service-container">
      <List sx={style} component="nav" aria-label="mailbox folders">
        {selectedServicesList?.map((service, index) => {
          // console.log(service);
          return (
            <ListItem key={index}>
              <ListItemText primary={service?.name} />
              <Button
                variant="outline"
                onClick={() => unselectService(service?.id)}
              >
                <CloseIcon />
              </Button>
            </ListItem>
          );
        })}
      </List>
      <Divider />
      <Paper style={{ maxHeight: 400, overflow: "auto" }}>
        <List sx={style} component="nav" aria-label="mailbox folders">
          {servicesList?.map((service, index) => {
            return (
              <ListItemButton onClick={() => selectService(service?.id)}>
                <ListItem key={index}>
                  <ListItemText primary={service?.name} />
                  <p style={{ color: "red" }}>
                    {currencyFormatter.format(service?.currentPrice)}{" "}
                  </p>
                </ListItem>
              </ListItemButton>
            );
          })}
        </List>
      </Paper>
    </div>
  );
};

export default BookingService;
