import "./booking-time.css"
import {
  FormControl,
  InputLabel,
  NativeSelect,
} from "@mui/material";
import { useContext } from "react";
import TimeList from "../../../../components/time-list/TimeList";
import { BookingContext } from "../../../../context/BookingContext";

const BookingTime = () => {
  const {
    // selectedBranch,
    // selectedServicesList,
    // timestampsList,
    handleChangeDay,
    currentDatesList
  } = useContext(BookingContext);

  return (
    <div className="time-container">
      <FormControl
        variant="standard"
        sx={{ m: 1, width: "100%", display: "flex", justifyContent: "center" }}
      >
        <InputLabel id="day-select">Ngày</InputLabel>
        <NativeSelect
          labelId="day-select"
          id="day-select"
          onChange={(e) => handleChangeDay(e)}
          defaultValue={0}
          label="Ngày"
          sx={{ textAlign: "center" }}
        >
          {currentDatesList.map((key, index) => {
            return <option value={index}>{key}</option>;
          })}
        </NativeSelect>
      </FormControl>
      {/* <TimeList list={currentDayTimestampsList} /> */}
      <TimeList />
    </div>
  );
};

export default BookingTime;