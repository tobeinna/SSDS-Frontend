import {
  FormControl,
  //   FormHelperText,
  //   Input,
  //   InputLabel,
  TextField,
  Typography,
} from "@mui/material";
import { Fragment, useContext, useState } from "react";
import { BookingContext } from "../../../../context/BookingContext";
import "./booking-info.css";
// import BookingStepper from "../../components/stepper/BookingStepper";

const BookingInfo = () => {
  const {
    nameState,
    setNameState,
    phoneState,
    setPhoneState,
    emailState,
    setEmailState,
    setIsNextable0,
  } = useContext(BookingContext);
  const [nameError, setNameError] = useState("");
  const [phoneError, setPhoneError] = useState("");
  const [emailError, setEmailError] = useState("");
  const nameRegex =
    /^[A-ZÀÁẠẢÃÂẦẤẬẨẪĂẰẮẶẲẴÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸĐ][a-zàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ]*(?:[ ][A-ZÀÁẠẢÃÂẦẤẬẨẪĂẰẮẶẲẴÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸĐ][a-zàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ]*)*$/;
  const phoneRegex =
    /(03|05|07|08|09|01[2|6|8|9]|\+843|\+845|\+847|\+848|\+849|\+841[2|6|8|9])+([0-9]{8})\b/;
  const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

  const checkNextable = () => {
    let currentNameError = nameError;
    let currentPhoneError = phoneError;
    let currentEmailError = emailError;
    let currentName = nameState;
    let currentPhone = phoneState;
    if (
      currentNameError === "" &&
      currentPhoneError === "" &&
      currentEmailError !== "Email không hợp lệ!" &&
      currentName !== "" &&
      currentPhone !== ""
    ) {
      setIsNextable0(true);
    } else {
      setIsNextable0(false);
    }
  };

  const validateName = (e, regex) => {
    if (e.target.value !== "") {
      if (regex.test(e.target.value)) {
        setNameError("");
        setNameState(e.target.value);
      } else {
        setNameError("Họ tên không hợp lệ!");
        setNameState(e.target.value);
      }
    } else {
      setNameError("Họ tên không được để trống!");
      setNameState("");
    }
    checkNextable();
  };

  const validatePhone = (e, regex) => {
    if (e.target.value !== "") {
      if (regex.test(e.target.value)) {
        setPhoneError("");
        setPhoneState(e.target.value);
      } else {
        setPhoneError("Số điện thoại không hợp lệ!");
        setPhoneState(e.target.value);
      }
    } else {
      setPhoneError("Số điện thoại không được để trống!");
      setPhoneState("");
    }
    checkNextable();
  };

  const validateEmail = (e, regex) => {
    if (e.target.value !== "") {
      if (regex.test(e.target.value)) {
        setEmailError("");
        setEmailState(e.target.value);
      } else {
        setEmailError("Email không hợp lệ!");
        setEmailState(e.target.value);
      }
    } else {
      setEmailError("");
      setEmailState("");
    }
    checkNextable();
  };

  return (
    <Fragment>
      <Typography sx={{display: 'flex', justifyContent: 'center'}} variant="h2" gutterBottom>
        Thông tin khách hàng
      </Typography>
      <div className="form-container">
        <FormControl className="form">
          <TextField
            className="form-input"
            id="name"
            label="Họ tên"
            variant="outlined"
            placeholder="Nhập họ tên khách hàng"
            size="small"
            value={nameState}
            onChange={(e) => {
              validateName(e, nameRegex);
            }}
            onBlur={checkNextable()}
          />
          <div className="error-container">
            {nameError && <div className="validate-error">{nameError}</div>}
          </div>
          <TextField
            className="form-input"
            id="phone"
            label="Điện thoại"
            value={phoneState}
            variant="outlined"
            placeholder="Nhập số điện thoại liên lạc"
            size="small"
            onChange={(e) => {
              validatePhone(e, phoneRegex);
            }}
            onBlur={checkNextable()}
          />
          <div className="error-container">
            {phoneError && <div className="validate-error">{phoneError}</div>}
          </div>
          <TextField
            className="form-input"
            id="email"
            label="Email (tùy chọn)"
            value={emailState}
            variant="outlined"
            placeholder="Nhập email"
            size="small"
            onChange={(e) => {
              validateEmail(e, emailRegex);
            }}
            onBlur={checkNextable()}
          />
          <div className="error-container">
            {emailError && <div className="validate-error">{emailError}</div>}
          </div>
        </FormControl>
      </div>
    </Fragment>
  );
};

export default BookingInfo;
