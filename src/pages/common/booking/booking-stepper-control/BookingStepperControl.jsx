import { Button } from "@mui/material";
import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { BookingContext } from "../../../../context/BookingContext";

const BookingStepperControl = () => {
    const navigate = useNavigate()
    const {activeStep, handleBack, handleNext, checkDisabled} = useContext(BookingContext)

    return ( 
        <div className="btn-container">
            {activeStep !== 0 ? (
              <Button
                onClick={handleBack}
                className="back-btn"
              >
                Back
              </Button>
            ) : (
              <p></p>
            )}

            {activeStep === 3 ? (
              <Button
                disabled={checkDisabled(activeStep)}
                color="primary"
                onClick={() => {
                    navigate("/booking-preview")
                    handleNext()}}
                className="next-btn"
              >
                Preview
              </Button>
            ) : (
              <Button
                disabled={checkDisabled(activeStep)}
                color="primary"
                onClick={handleNext}
                className="next-btn"
              >
                Next
              </Button>
            )}
          </div>
     );
}
 
export default BookingStepperControl;