import { Fragment, useContext } from "react";
import SearchBar from "../../../../components/search-bar/SearchBar";
import BookingBranchList from "../../../../components/booking-branch-list/BookingBranchList";
import "./booking-branch.css";
import { Button } from "@mui/material";
import MyLocationIcon from "@mui/icons-material/MyLocation";
// import axios from "axios";
// import goongJs from "@goongmaps/goong-js";
import { BookingContext } from "../../../../context/BookingContext";

const BookingBranch = () => {

  const {branchesState, getLocation} = useContext(BookingContext)

  return (
    <Fragment>
      <div className="booking-branch-container">
        <SearchBar
          label="Tìm chi nhánh"
          placeholder="Nhập chi nhánh mà bạn muốn tìm"
        />
        <Button size="small" variant="outlined" onClick={getLocation}>
          <MyLocationIcon />
          Tìm spa gần nhất
        </Button>
        <BookingBranchList branches={branchesState} />
      </div>
    </Fragment>
  );
};

export default BookingBranch;
