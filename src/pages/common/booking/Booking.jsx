import { Typography } from "@material-ui/core";
import { Step, StepLabel, Stepper } from "@mui/material";
import { useContext } from "react";
import { BookingContext } from "../../../context/BookingContext";
// import { BookingProvider } from "../../../context/BookingContext";
import BookingBranch from "./booking-branch/BookingBranch";
import BookingInfo from "./booking-info/BookingInfo";
import BookingService from "./booking-service/BookingService";
import BookingStepperControl from "./booking-stepper-control/BookingStepperControl";
import BookingTime from "./booking-time/BookingTime";
import "./booking.css";

function getSteps() {
  return [
    "Thông tin khách hàng",
    "Chọn chi nhánh",
    "Chọn dịch vụ",
    "Chọn thời gian",
  ];
}

function getStepContent(step) {
  switch (step) {
    case 0:
      return <BookingInfo />;
    case 1:
      return <BookingBranch />;
    case 2:
      return <BookingService />;
    case 3:
      return <BookingTime />;
    default:
      return;
  }
}

const Booking = () => {
  const { activeStep } =
    useContext(BookingContext);

  const steps = getSteps();
  // const [activeStep, setActiveStep] = useState(0);

  // const handleNext = () => {
  //   setActiveStep((prevActiveStep) =>
  //     activeStep < 3 ? prevActiveStep + 1 : prevActiveStep
  //   );
  // };

  // const handleBack = () => {
  //   setActiveStep((prevActiveStep) =>
  //     activeStep > 0 ? prevActiveStep - 1 : prevActiveStep);
  // };

  return (
    // <BookingProvider>
    <div className="container">
      <Stepper
        className="stepper"
        nonLinear
        activeStep={activeStep}
        alternativeLabel
      >
        {steps.map((label, index) => {
          const stepProps = {};
          const labelProps = {};
          return (
            <Step key={index} {...stepProps}>
              <StepLabel {...labelProps}>{label}</StepLabel>
            </Step>
          );
        })}
      </Stepper>
      <div>
        <div>
          <Typography>{getStepContent(activeStep)}</Typography>
          <BookingStepperControl />
        </div>
      </div>
    </div>
    // </BookingProvider>
  );
};

export default Booking;
