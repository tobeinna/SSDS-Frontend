import { Button, Typography } from "@mui/material";
import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { BookingContext } from "../../../../context/BookingContext";
import "./booking-preview.css";

const BookingPreview = () => {
  const navigate = useNavigate();
  const {
    setActiveStep,
    nameState,
    phoneState,
    emailState,
    selectedBranch,
    selectedServicesList,
    selectedTimestamp,
  } = useContext(BookingContext);
  const currentName = nameState;
  const currentPhone = phoneState;
  const currentEmail = emailState;
  const currentBranch = selectedBranch;
  const currentServices = [...selectedServicesList];
  const currentTimestamp = selectedTimestamp;
  console.log(currentName);
  console.log(currentPhone);
  console.log(currentEmail);
  console.log(currentBranch);
  console.log(currentServices);
  console.log(currentTimestamp);

  const currencyFormatter = new Intl.NumberFormat("vi-VN", {
    style: "currency",
    currency: "VND",
  });

  var totalPrice = 0;
  var date = new Date(currentTimestamp).toLocaleDateString("vi-VN");
  var time = new Date(currentTimestamp).toLocaleTimeString("vi-VN");
  var timeString = String(date) + " " + String(time);

  return (
    <>
      <Typography
        sx={{ display: "flex", justifyContent: "center" }}
        variant="h2"
        gutterBottom
      >
        Xác nhận thông tin lịch hẹn
      </Typography>
      <div className="preview-info-container">
        <p>
          <strong>Tên khách hàng: </strong>
          {currentName}
        </p>
        <p>
          <strong>Điện thoại: </strong>
          {currentPhone}
        </p>
        <p style={{ display: currentEmail === "" ? "none" : "inline-block" }}>
          <strong>Email: </strong>
          {currentEmail}
        </p>
        <p>
          <strong>Tên chi nhánh: </strong>
          {currentBranch.name} - <strong>Điện thoại: </strong>
          {currentBranch.hotline}
        </p>
        <p>{currentBranch.detailAddress}</p>
        <p>Danh sách dịch vụ</p>
        <ol>
          {currentServices.map((item, index) => {
            totalPrice += item.currentPrice;
            return (
              <li className="service-preview-container" key={index}>
                <div>{item.name}</div>
                <div className="price-container">
                  {currencyFormatter.format(item.currentPrice)}
                </div>
              </li>
            );
          })}
        </ol>
        <p>
          <strong>Tổng chi phí (dự tính): </strong>
          <span className="price-container">
            {currencyFormatter.format(totalPrice)}
          </span>
        </p>
        <p>
          <strong>Thời gian hẹn (dự tính): </strong>
          {timeString}
        </p>
      </div>
      <div className="btn-container">
        <Button onClick={() => {
            setActiveStep(3)
            navigate("/booking")}} className="back-btn">
          Back
        </Button>
        <Button
          color="primary"
          onClick={() => navigate("/booking-finished")}
          className="next-btn"
        >
          Submit
        </Button>
      </div>
    </>
  );
};

export default BookingPreview;
