import { Grid } from "@mui/material";
import { Fragment } from "react";
import HomepageCarousel from "../../../components/carousel/HomepageCarousel";
import PostCarousel from "../../../components/carousel/PostCarousel";
import VerticalCategory from "../../../components/category/VerticalCategory";
// import Footer from "../../components/footer/Footer";
// import Header from "../../components/header/Header";
import SearchBar from "../../../components/search-bar/SearchBar";
import HomepageServiceList from "../../../components/service-list/HomepageServiceList";

///import Sidebar from "../../components/sidebar/Sidebar";

const Homepage = () => {
  return (
    <>
      <Fragment>
      <HomepageCarousel />
      </Fragment>
      <Fragment>
        <Grid container spacing={5} justifyContent="space-around" mt={4} >
          <Grid item xs={12} md={3} >
            <Grid item xs={12} >
            <SearchBar label="Tìm dịch vụ" placeholder="Nhập dịch vụ bạn muốn tìm" />
            </Grid>
            <Grid item display={{xs: "none", md: "block"}}>
            <VerticalCategory xs={0}/>
            </Grid>
          </Grid>
          <Grid item sm={12} md={9}>
            <HomepageServiceList />
          </Grid>
        </Grid>
      </Fragment>
      <Fragment>
        <PostCarousel />
      </Fragment>
    </>
  );
};

export default Homepage;
