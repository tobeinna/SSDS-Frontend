import React, { useState } from "react";
import './login.css';

const Login = () => {

    const [email, setEmail] = useState('');
    const [emailError, setEmailError] = useState('');
    const [password, setPassword] = useState('');
    const [passwordError, setPasswordError] = useState('');
    // const [successmess, setSuccessmess] = useState('');


    const handleEmailChange = (e) => {
        // setSuccessmess('');
        setEmailError('');
        setEmail(e.target.value);
    }

    const handlePasswordChange = (e) => {
        // setSuccessmess('');
        setPasswordError('');
        setPassword(e.target.value);
    }

    const handleFormSubmit = (e) => {
        e.preventDefault();

        if (email !== '') {
            const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
            if (emailRegex.test(email)) {
                setEmailError('');
                if (email === 'admin@demo.com') {
                    setEmailError('');
                    if (password === 'demo') {
                        setEmailError('');
                    } else {
                        setEmailError('Password does not match');
                    }
                } else {
                    setEmailError('Email does not match');
                }
            }
            else {
                setEmailError('Email Invalid');
            }
        } else {
            setEmailError('Email Required');
        }

        if (password !== '') {

        } else {
            setPasswordError('Password Required');
        }
    }


    return (
       
            <form className="cover" onSubmit={handleFormSubmit}>
                <h1 size="50px">Đăng Nhập</h1>
                <div className="input">
                    <input size="75" type="text" placeholder="Nhập email" onChange={handleEmailChange} value={email} />
                    <br></br>
                    {emailError && <div className="error-msg">{emailError}</div>}
                </div>
                <div className="input">
                    <input size="75" type="password" placeholder="Nhập mật khẩu" onChange={handlePasswordChange} value={password} />
                    <br></br>
                    {passwordError && <div className="error-msg">{passwordError}</div>}
                </div>
                <div className="container_swap">
                    <button to="/register" className="register-btn" variant="contained" >Đăng kí</button>
                    <button to="/forget" className="forget-btn" variant="contained" >Quên mật khẩu</button>
                </div>
                <button className="login-btn">Đăng nhập</button>
            </form>
       
    );
}

export default Login