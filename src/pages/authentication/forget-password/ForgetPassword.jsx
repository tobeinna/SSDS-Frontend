import React from "react";
import "./forgetpassword.css";


const ForgetPassword = () => {
    return (
        <div className="cover">
            <h1 size="50px">Lấy lại mật khẩu</h1>
            <div className="input">
                <input size="75" type="email" placeholder="Nhập email" />
                <i className="fa-user-circle-o"></i>
            </div>
            <div>
                    <button className="forget-btn">Lấy lại mật khẩu</button>
            </div>
        </div>
    );
}




export default ForgetPassword