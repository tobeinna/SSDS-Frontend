import React,{ useState } from "react";
import "./register.css"

const Register = () => {

    const [email, setEmail] = useState('');
    const [emailError, setEmailError] = useState('');
    const [password, setPassword] = useState('');
    const [phone, setPhone] = useState('');
    const [phoneError, setPhoneError] = useState('');
    const [name, setName] = useState('');
    const [nameError, setNameError] = useState('');
    const [passwordError, setPasswordError] = useState('');
    // const [successmess, setSuccessmess] = useState('');


    const handleEmailChange = (e) => {
        // setSuccessmess('');
        setEmailError('');
        setEmail(e.target.value);
    }

    const handlePasswordChange = (e) => {
        // setSuccessmess('');
        setPasswordError('');
        setPassword(e.target.value);
    }

    const handleNameChange = (e) => {
        // setSuccessmess('');
        setNameError('');
        setName(e.target.value);
    }

    const handlePhoneChange = (e) => {
        // setSuccessmess('');
        setPhoneError('');
        setPhone(e.target.value);
    }

    const handleFormSubmit = (e) => {
        e.preventDefault();

        if (email !== '') {
            const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
            if (emailRegex.test(email)) {
                setEmailError('');
                if (email === 'admin@demo.com') {
                    setEmailError('');
                    if (password === 'demo') {
                        setEmailError('');
                    } else {
                        setEmailError('Password does not match');
                    }
                } else {
                    setEmailError('Email does not match');
                }
            }
            else {
                setEmailError('Email Invalid');
            }
        } else {
            setEmailError('Email Required');
        }

        if (password !== '') {
            
        } else {
            setPasswordError('Password Required');
        }

        if (phone !== '') {
            const phoneregex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
            if (phoneregex.test(email)) {
                setPhoneError('Phone does not match');
            }
        } else {
            setPhoneError('Phone Required');
        }

        if (name !== '') {

        } else {
            setNameError('Name Required');
        }
    }

    return (
        <form className="cover" onSubmit={handleFormSubmit}>
            <h1>Đăng Kí</h1>
            <div className="input">
                <input size="60" type="text" placeholder="Nhập Họ Tên" onChange={handleNameChange} value={name} />
                <br></br>
                {nameError && <div className="error-msg">{nameError}</div>}
            </div>
            <div className="input">
                <input size="60" type="text" placeholder="Nhập Số điện thoại" onChange={handlePhoneChange} value={phone} />
                <br></br>
                {phoneError && <div className="error-msg">{phoneError}</div>}
            </div>
            <div className="input">
                <input size="60" type="email" placeholder="Nhập email" onChange={handleEmailChange} value={email} />
                <br></br>
                {emailError && <div className="error-msg">{emailError}</div>}
            </div>
            <div className="input">
                <input size="60" type="password" placeholder="Nhập mật khẩu" onChange={handlePasswordChange} value={password} />
                <br></br>
                {passwordError && <div className="error-msg">{passwordError}</div>}
            </div>
            <div className="input">
                <input size="60" type="password" placeholder="Nhập lại mật khẩu" onChange={handlePasswordChange} value={password} />
                <br></br>
                {passwordError && <div className="error-msg">{passwordError}</div>}
            </div>
 
            <button className="register-btn">Đăng Kí</button>
        
        </form>
    );
}


export default Register