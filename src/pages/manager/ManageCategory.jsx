import React, {useEffect, useState} from "react";
import axios from 'axios';
import { Button } from "@mui/material";
import TextField from "@mui/material/TextField";
import Table from "../components/table/Table";
import './Manage.css'


export const ManageCategory = () => {

  const [dataTable, setDataTable] = useState([]);
  

  useEffect(() =>{
      axios.get('http://localhost:8088/api/web/v1/category')

      .then(res => {
        console.log(res)
        setDataTable(res.data.data)
      })
      .catch(err => console.log(err))
     
  }, []);

  const column = [
    { heading: 'id', value: 'id' },
    { heading: 'name', value: 'name' },
    { heading: 'code', value: 'code' },
    { heading: 'description', value: 'description' },
    { heading: 'Spa Services', value: 'spa_services' },
  ]

    return (

      <div className='category'>
       
      <TextField
          hiddenLabel
          id="filled-hidden-label-small"
          placeholder="Tìm kiếm"
          variant="outlined"
          size="big"
        
        />
        
      <Button className="add_cate" variant="contained" color="primary">Thêm mới</Button>
        <div>
          <h1>Bảng dữ liệu các loại dịch vụ</h1>
        </div>
       <Table data={dataTable} column={column}/>
      </div>
    );
  };

  export default ManageCategory;