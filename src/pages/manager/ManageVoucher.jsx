import React, {useEffect, useState} from "react";
import axios from 'axios';
import { Button } from "@mui/material";
import TextField from "@mui/material/TextField";
import TableVoucher from "../components/table/TableVoucher"

export const ManageVoucher = () => {

    const [dataTable, setDataTable] = useState([]);
  

    useEffect(() =>{
        axios.get('')
  
        .then(res => {
          console.log(res)
          setDataTable(res.data.data)
        })
        .catch(err => console.log(err))
       
    }, []);

    const column = [
        { heading: 'id', value: 'id' },
        { heading: 'title', value: 'title' },
        { heading: 'code', value: 'code' },
        { heading: 'description', value: 'description' },
        { heading: 'type', value: 'type' },
        { heading: 'price', value: 'price' },
        { heading: 'startdate', value: 'startdate' },
        { heading: 'enddate', value: 'enddate' },
        { heading: 'quantity', value: 'quantity' },

      ]
    
        return (
    
          <div className='voucher'>
           
          <TextField
              hiddenLabel
              id="filled-hidden-label-small"
              placeholder="Tìm kiếm"
              variant="outlined"
              size="big"
            
            />
            
          <Button className="add_voucher" variant="contained" color="primary">Thêm mới</Button>
            <div>
              <h1>Bảng dữ liệu các mã giảm giá</h1>
            </div>
           <TableVoucher data={dataTable} column={column}/>
          </div>
        );

}

export default ManageVoucher;