import React, {useEffect, useState} from "react";
import axios from 'axios';
import { Button } from "@mui/material";
import TextField from "@mui/material/TextField";
import TableService from "../components/table/TableService"

export const ManageService  = () => {

    const [dataTable, setDataTable] = useState([]);
  

    useEffect(() =>{
        axios.get('')
  
        .then(res => {
          console.log(res)
          setDataTable(res.data.data)
        })
        .catch(err => console.log(err))
       
    }, []);

    const column = [
        { heading: 'id', value: 'id' },
        { heading: 'name', value: 'name' },
        { heading: 'code', value: 'code' },
        { heading: 'description', value: 'description' },
        { heading: 'isActive', value: 'isActive' },
        { heading: 'categoryName', value: 'categoryName' },
        { heading: 'equimentName', value: 'equimentName' },
        { heading: 'currentPrice', value: 'currentPrice' },
      ]
    
        return (
    
          <div className='service'>
           
          <TextField
              hiddenLabel
              id="filled-hidden-label-small"
              placeholder="Tìm kiếm"
              variant="outlined"
              size="big"
            
            />
            
          <Button className="add_ser" variant="contained" color="primary">Thêm mới</Button>
            <div>
              <h1>Bảng dữ liệu các Dịch Vụ</h1>
            </div>
           <TableService data={dataTable} column={column}/>
          </div>
        );

}

export default ManageService;