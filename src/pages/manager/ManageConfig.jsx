import React, {useEffect, useState} from "react";
import axios from 'axios';
import { Button } from "@mui/material";
import TextField from "@mui/material/TextField";
import TableConfig from "../components/table/TableConfig";

export const ManageConfig = () => {

    const [dataTable, setDataTable] = useState([]);
  

    useEffect(() =>{
        axios.get('')
  
        .then(res => {
          console.log(res)
          setDataTable(res.data.data)
        })
        .catch(err => console.log(err))
       
    }, []);

    const column = [

        { heading: 'id', value: 'id' },
        { heading: 'name', value: 'name' },
        { heading: 'code', value: 'code' },


      ]
    
        return (
    
          <div className='config'>
           
          <TextField
              hiddenLabel
              id="filled-hidden-label-small"
              placeholder="Tìm kiếm"
              variant="outlined"
              size="big"
            
            />
            
          <Button className="add_config" variant="contained" color="primary">Thêm mới</Button>
            <div>
              <h1>Bảng dữ liệu các cấu hình</h1>
            </div>
           <TableConfig data={dataTable} column={column}/>
          </div>
        );

}

export default ManageConfig;