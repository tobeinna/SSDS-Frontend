import React, {useEffect, useState} from "react";
import axios from 'axios';
import { Button } from "@mui/material";
import TextField from "@mui/material/TextField";
import Tablebranch from "../components/table/TableBrach";

export const ManageBranch = () => {

    const [dataTable, setDataTable] = useState([]);
  

    useEffect(() =>{
        axios.get('')
  
        .then(res => {
          console.log(res)
          setDataTable(res.data.data)
        })
        .catch(err => console.log(err))
       
    }, []);

    const column = [
        { heading: 'id', value: 'id' },
        { heading: 'name', value: 'name' },
        { heading: 'code', value: 'code' },
        { heading: 'detailAddress', value: 'detailAddress' },
        { heading: 'latitude', value: 'latitude' },
        { heading: 'longitude', value: 'longitude' },
        { heading: 'hotline', value: 'hotline' },

      ]
    
        return (
    
          <div className='category'>
           
          <TextField
              hiddenLabel
              id="filled-hidden-label-small"
              placeholder="Tìm kiếm"
              variant="outlined"
              size="big"
            
            />
            
          <Button className="add_branch" variant="contained" color="primary">Thêm mới</Button>
            <div>
              <h1>Bảng dữ liệu các Cơ sở</h1>
            </div>
           <Tablebranch data={dataTable} column={column}/>
          </div>
        );

}

export default ManageBranch;