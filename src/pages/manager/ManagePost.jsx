import React, {useEffect, useState} from "react";
import axios from 'axios';
import { Button } from "@mui/material";
import TextField from "@mui/material/TextField";
import TablePost from "../components/table/TablePost"

export const ManagePost = () => {

    const [dataTable, setDataTable] = useState([]);
  

    useEffect(() =>{
        axios.get('')
  
        .then(res => {
          console.log(res)
          setDataTable(res.data.data)
        })
        .catch(err => console.log(err))
       
    }, []);

    const column = [
        { heading: 'id', value: 'id' },
        { heading: 'title', value: 'title' },
        { heading: 'code', value: 'code' },

      ]
    
        return (
    
          <div className='post'>
           
          <TextField
              hiddenLabel
              id="filled-hidden-label-small"
              placeholder="Tìm kiếm"
              variant="outlined"
              size="big"
            
            />
            
          <Button className="add_equi" variant="contained" color="primary">Thêm mới</Button>
            <div>
              <h1>Bảng dữ liệu các Bài Đăng</h1>
            </div>
           <TablePost data={dataTable} column={column}/>
          </div>
        );

}

export default ManagePost;