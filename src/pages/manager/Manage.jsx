import React from 'react';
import { ManageCategory } from './ManageCategory';
import { ManageBranch } from './ManageBranch';
import { ManageService }  from './ManageService';
import { ManageEquiment } from './ManageEquiment';
import { ManagePost } from './ManagePost';
import { ManageVoucher } from './ManageVoucher';
import { ManageConfig } from './ManageConfig';
import { ManageSCA } from './ManageSCA';



export const Manage = () => {
  return (
    <div className='bento'>
      <ManageCategory/>
    </div>
  );
};



export const ManageTwo = () => {
  return (
    <div className='bento'>
      <ManageBranch/>
    </div>
  );
};

export const ManageThree = () => {
  return (
    <div className='bento'>
      
      <ManageService/>
    </div>
  );
};

export const ManageFour = () => {
  return (
    <div className='bento'>
      <ManageEquiment/>
    </div>
  );
};

export const ManageFive = () => {
  return (
    <div className='bento'>
      <ManagePost/>
    </div>
  );
};

export const ManageSix = () => {
  return (
    <div className='bento'>
      
      <ManageConfig/>
    </div>
  );
};

export const ManageSeven = () => {
  return (
    <div className='bento'>
      <ManageVoucher/>
    </div>
  );
};

export const ManageEight = () => {
  return (
    <div className='bento'>
      <ManageSCA/>
    </div>
  );
};

export const ManageNine = () => {
  return (
    <div className='bento'>
      
      <h1>Reports/reports3</h1>
    </div>
  );
};

