import React, {useEffect, useState} from "react";
import axios from 'axios';
import { Button } from "@mui/material";
import TextField from "@mui/material/TextField";
import TableEquiment from "../components/table/TableEquiment"

export const ManageEquiment = () => {

    const [dataTable, setDataTable] = useState([]);
  

    useEffect(() =>{
        axios.get('')
  
        .then(res => {
          console.log(res)
          setDataTable(res.data.data)
        })
        .catch(err => console.log(err))
       
    }, []);

    const column = [
        { heading: 'id', value: 'id' },
        { heading: 'name', value: 'name' },
        { heading: 'code', value: 'code' },
        { heading: 'description', value: 'description' },

      ]
    
        return (
    
          <div className='equipment'>
           
          <TextField
              hiddenLabel
              id="filled-hidden-label-small"
              placeholder="Tìm kiếm"
              variant="outlined"
              size="big"
            
            />
            
          <Button className="add_equi" variant="contained" color="primary">Thêm mới</Button>
            <div>
              <h1>Bảng dữ liệu các Thiết Bị</h1>
            </div>
           <TableEquiment data={dataTable} column={column}/>
          </div>
        );

}

export default ManageEquiment;