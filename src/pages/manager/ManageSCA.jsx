import React, {useEffect, useState} from "react";
import axios from 'axios';
import { Button } from "@mui/material";
import TextField from "@mui/material/TextField";
import TableSCA from "../components/table/TableSCA";
import './Manage.css'


export const ManageSCA = () => {

  const [dataTable, setDataTable] = useState([]);
  

  useEffect(() =>{
      axios.get('')

      .then(res => {
        console.log(res)
        setDataTable(res.data.data)
      })
      .catch(err => console.log(err))
     
  }, []);

  const column = [
    { heading: 'id', value: 'id' },
    { heading: 'question', value: 'question' },
    { heading: 'order', value: 'order' },
    { heading: 'isRequire', value: 'isRequire' },
    { heading: 'Form', value: 'Form' },
  ]

    return (

      <div className='SCAquestion'>
       
      <TextField
          hiddenLabel
          id="filled-hidden-label-small"
          placeholder="Tìm kiếm"
          variant="outlined"
          size="big"
        
        />
        
      <Button className="add_sca" variant="contained" color="primary">Thêm mới</Button>
        <div>
          <h1>Bảng dữ liệu các câu hỏi khảo sát</h1>
        </div>
       <TableSCA data={dataTable} column={column}/>
      </div>
    );
  };

  export default ManageSCA;