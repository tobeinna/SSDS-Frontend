import Homepage from "../pages/common/homepage/Homepage";
import Register from "../pages/authentication/register/Register";
import ForgetPassword from "../pages/authentication/forget-password/ForgetPassword";
import Login from "../pages/authentication/login/Login";
import Booking from "../pages/common/booking/Booking";
import BookingPreview from "../pages/common/booking/booking-preview/BookingPreview";
import BookingFinished from "../pages/common/booking/booking-finished/BookingFinished";

const publicRoutes = [
    {path: "/", component: Homepage },
    {path: "/login", component: Login, layout: null },
    {path: "/register", component: Register, layout: null },
    {path: "/forget-password", component: ForgetPassword, layout: null },
    {path: "/booking", component: Booking },
    {path: "/booking-preview", component: BookingPreview },
    {path: "/booking-finished", component: BookingFinished },
]

const privateRoutes = []

export { publicRoutes, privateRoutes }