import axios from "axios";
import { useEffect } from "react";
import { useState } from "react";
import { createContext } from "react";

export const BookingContext = createContext({});

export const BookingProvider = ({ children }) => {
  //StepControl

  const [activeStep, setActiveStep] = useState(0);
  const [isNextable0, setIsNextable0] = useState(false);
  const [isNextable1, setIsNextable1] = useState(false);
  const [isNextable2, setIsNextable2] = useState(false);
  const [isFinishable, setIsFinishable] = useState(false);

  const checkDisabled = (step) => {
    var isDisabled = true
    switch (step) {
      case 0:
        isNextable0 === true ? isDisabled = false :  isDisabled = true
        break;
      case 1:
        isNextable1 === true ? isDisabled = false :  isDisabled = true
        break;
      case 2:
        isNextable2 === true ? isDisabled = false :  isDisabled = true
        break;
      case 3:
        isFinishable === true ? isDisabled = false :  isDisabled = true
        break;
    
      default:
        break;
    }

    return isDisabled;
  }

  const handlePreview = () => {
    console.log(nameState);
    console.log(phoneState);
    console.log(emailState);
    console.log(selectedBranch);
    console.log(selectedServicesList);
    console.log(selectedTimestamp);
  }


  const handleNext = () => {
    setActiveStep((prevActiveStep) =>
      activeStep < 3 ? prevActiveStep + 1 : prevActiveStep
    );
    if(activeStep === 3) handlePreview()
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) =>
      activeStep > 0 ? prevActiveStep - 1 : prevActiveStep
    );
  };

  //BookingInfo

  const [nameState, setNameState] = useState('');
  const [phoneState, setPhoneState] = useState('');
  const [emailState, setEmailState] = useState('');

  //BookingBranch

  var sortedBranches;
  const [selectedBranch, setSelectedBranch] = useState();
  const [isSorted, setIsSorted] = useState(false);
  const [branchesState, setBranchesState] = useState();

  useEffect(() => {
    setIsSorted(false);
    const getBranches = async () => {
      try {
        const res = await axios.get(`http://localhost:8088/api/web/v1/branch`);
        setBranchesState(res.data.data);
      } catch (error) {
        console.log(error.message);
      }
    };
    getBranches();
  }, []);

  useEffect(() => {
    if (isSorted && sortedBranches) {
      console.log(sortedBranches);
      setBranchesState(sortedBranches);
    }
  }, [isSorted, sortedBranches]);

  const getDistance = async (lat, long) => {
    var spaDestination = "";
    branchesState.map((item, index, arr) => {
      spaDestination += item.latitude + "," + item.longitude;
      if (arr.length - 1 > index) {
        spaDestination += "%7C";
      }
      return null;
    });
    try {
      const res = await axios.get(
        `https://rsapi.goong.io/DistanceMatrix?origins=` +
          lat +
          "," +
          long +
          "&destinations=" +
          spaDestination +
          "&api_key=O8C34vNPiWUblu2Zhk76cGwiABK9WCj3Jtsqmkeu"
      );
      var spaDistance = res.data.rows[0].elements;
      branchesState.map((item, index) => {
        item.distance = spaDistance[index].distance;
        return null;
      });
      sortedBranches = branchesState;
      sortedBranches.sort((a, b) => a.distance.value - b.distance.value);
      console.log(sortedBranches);
      setIsSorted(true);
    } catch (error) {
      console.log(error.message);
    }
  };

  const getLocation = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        getDistance(position.coords.latitude, position.coords.longitude);
      });
    }
    console.log(branchesState);
  };

  //BookingService

  const [servicesState, setServicesState] = useState([]);
  const [servicesList, setServicesList] = useState([]);
  const [selectedServicesList, setSelectedServicesList] = useState([]);

  useEffect(() => {
    const getServices = async () => {
      try {
        const res = await axios.get(`http://localhost:8088/api/web/v1/service`);
        setServicesState(res.data.data);
        setServicesList(res.data.data);
      } catch (error) {
        console.log(error.message);
      }
    };
    getServices();
  }, []);

  const selectService = (id) => {
    let tempSelectedList = [...selectedServicesList];
    let tempServicesList = [...servicesList];
    const item = servicesState.find((x) => x.id === id);
    tempSelectedList.push(item);
    setSelectedServicesList(tempSelectedList);
    const index = tempServicesList.indexOf(
      servicesList.find((x) => x.id === id)
    );
    if (index > -1) {
      tempServicesList.splice(index, 1);
    }
    setServicesList(tempServicesList);
    setIsNextable2(true);
    // console.log(selectedServicesList);
    var tempIsFinishable = isFinishable
    if(tempIsFinishable) setIsFinishable(false);
  };

  const unselectService = (id) => {
    let tempSelectedList = [...selectedServicesList];
    let tempServicesList = [...servicesList];
    const item = selectedServicesList.find((x) => x.id === id);
    tempServicesList.push(item);
    tempServicesList.sort((a, b) => a.id - b.id);
    setServicesList(tempServicesList);
    const index = tempSelectedList.indexOf(
      selectedServicesList.find((x) => x.id === id)
    );
    if (index > -1) {
      tempSelectedList.splice(index, 1);
    }
    setSelectedServicesList(tempSelectedList);
    if(tempSelectedList.length === 0) {
      setIsNextable2(false);
    }
    var tempIsFinishable = isFinishable
    if(tempIsFinishable) setIsFinishable(false);
    // console.log("servicesList");
    // console.log(tempServicesList);
    console.log(selectedServicesList);
  };

  //BookingTime

  const [timestampsList, setTimestampsList] = useState([]);
  // const [currentDayTimestampsList, setCurrentDayTimestampsList] = useState();
  // const [currentTimestampsList, setCurrentTimestampsList] = useState();

  useEffect(() => {
    const getTimestamps = async () => {
      var currentBranchCode;
      var tempSelectedServicesList;
      var selectedServicesListId = [];

      currentBranchCode = selectedBranch?.code;
      tempSelectedServicesList = [...selectedServicesList];
      console.log(tempSelectedServicesList);
      tempSelectedServicesList.map((item) => {
        selectedServicesListId.push(item.id);
        return null;
      });
      console.log(currentBranchCode);
      console.log(selectedServicesListId);

      var data = JSON.stringify({
        branchCode: currentBranchCode,
        listServicesId: selectedServicesListId,
      });

      var config = {
        method: "post",
        url: "http://localhost:8088/api/web/v1/appointment-tracking/available-time",
        headers: {
          "Content-Type": "application/json",
        },
        data: data,
      };

      axios(config)
        .then(function (response) {
          setTimestampsList(response.data.data);
          console.log(response.data.data);
        })
        .catch(function (error) {
          console.log(error);
        });
    };
    getTimestamps();
  }, [selectedBranch, selectedServicesList, servicesList]);

  let tempTimeStampsList = timestampsList;

  var currentTimestampsList = [];
  var currentDatesList = [];

  const groupByDate = (list) => {
    return list?.reduce((groups, timestamp) => {
      const date = new Date(timestamp).toLocaleDateString("vi-VN", {
        weekday: "long",
        year: "numeric",
        month: "long",
        day: "numeric",
      });
      // console.log("list", list);
      if (!groups.hasOwnProperty(date)) {
        groups[date] = [];
      }
      const time = new Date(timestamp).toLocaleTimeString("vi-VN", {
        hour: "2-digit",
        minute: "2-digit",
      });
      groups[date].push({
        timestamp: timestamp,
        time: time,
      });
      return groups;
    }, {});
  };

  var data = groupByDate(tempTimeStampsList);

  for (var property in data) {
    if (!data.hasOwnProperty(property)) {
      continue;
    }

    currentDatesList.push(property);
    currentTimestampsList.push(data[property]);
  }

  function createInitialTimestampsList() {
    return currentTimestampsList[0];
  }

  const [timestampState, setTimestampState] = useState(
    createInitialTimestampsList
  );
  const [selectedTimestamp, setSelectedTimestamp] = useState(0);

  const handleChangeDay = (e) => {
    setTimestampState(currentTimestampsList[[e.target.value]]);
  };

  return (
    <BookingContext.Provider
      value={{
        activeStep,
        handleNext,
        handleBack,
        checkDisabled,
        nameState,
        setNameState,
        phoneState,
        setPhoneState,
        emailState,
        setEmailState,
        setIsNextable0,
        setIsNextable1,
        branchesState,
        getLocation,
        selectedBranch,
        setSelectedBranch,
        selectedServicesList,
        servicesState,
        setSelectedServicesList,
        unselectService,
        servicesList,
        setServicesList,
        selectService,
        timestampsList,
        setTimestampsList,
        handleChangeDay,
        currentDatesList,
        // currentDayTimestampsList,
        timestampState,
        selectedTimestamp,
        setSelectedTimestamp,
        setIsFinishable,
        // currentDayTimestampsList,
        // setCurrentDayTimestampsList
        setActiveStep
      }}
    >
      {children}
    </BookingContext.Provider>
  );
};
